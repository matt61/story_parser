A simple package for parsing narrative structures

## Setup

    pip3 install virtualenv

For local environment you may want to use virtualenv

    virtualenv -p python3 env && source env/bin/activate

To Test:

    pip install pytest

Then

    pytest test