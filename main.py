import argparse
import pprint
from story_parser import parse

if __name__ == '__main__':
    parser = argparse.ArgumentParser('Parse a story')
    parser.add_argument('file', type=argparse.FileType('r'))
    parser.add_argument('wrapped', type=bool, default=False)
    args = parser.parse_args()
    content = args.file.read().strip()

    if args.wrapped:
        encoded = parse(content.replace("\n\n", "||").replace("\n", " ").replace("||", "\n"))
    else:
        encoded = parse(content)

    pp = pprint.PrettyPrinter(indent=4, compact=False, width=160)
    pp.pprint(encoded)
