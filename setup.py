import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="story_parser-matt61",
    version="1.0",
    author="Matt Ward",
    author_email="matt@matthewward.net",
    description="A simple package for parsing stories",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/matt61/story_parser",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)