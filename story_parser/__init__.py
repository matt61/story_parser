import json

from story_parser.models import Root
from story_parser.serializers import StorySerializer


def parse(content_: str):
    story = Root(content=Root.charize(content_))
    return json.loads(StorySerializer().encode(story.parts))
