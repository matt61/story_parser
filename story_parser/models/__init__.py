import abc
import textwrap
from typing import List, Iterator, Dict, Type, Tuple
from story_parser.functions import contains


class Part(abc.ABC):
    def __eq__(self, other):
        return self.__hash__() == other.__hash__()

    @abc.abstractmethod
    def __hash__(self):
        pass


class ComplexPart(Part, abc.ABC):

    @classmethod
    @abc.abstractmethod
    def allowed(cls):
        pass

    def __init__(self, content, modifier=None):
        assert isinstance(content, list), f"""
            Must pass list got {type(content)}, got {content}
        """
        self.content = content
        self.modifier = modifier

    @property
    @abc.abstractmethod
    def parts(self) -> List['Part']:
        pass

    def __hash__(self):
        return hash(tuple(x.__hash__() for x in self.content))

    def __repr__(self):
        return f"{self.__class__.__name__} {self.content}"


class SimpleSplit(ComplexPart, abc.ABC):
    pass


class Char(Part):

    def __init__(self, value):
        assert isinstance(value, str)
        self.value = value

    def __hash__(self):
        return hash(self.value)

    def __repr__(self):
        if self.value == "\n":
            return "New Line"
        if self.value == " ":
            return "Space"
        return self.value


class Splitter:
    def __init__(self, chars: Iterator['Char'], modifier: str = None):
        assert isinstance(modifier, str) or modifier is None
        assert isinstance(chars, (list, tuple))
        assert all(isinstance(char, Char) for char in chars)

        self.chars = list(chars)
        self.modifier = modifier


class Narrative(ComplexPart, abc.ABC):

    @classmethod
    def allowed(cls):
        return Paragraph, Sentence, Word, Narrative, Char

    @property
    def parts(self):
        return WordFactory.extract(
            SemiSentenceFactory.extract(
                SentenceFactory.extract(
                    ParagraphFactory.extract(
                        SubNarrativeFactory.extract(
                            self.content
                        )
                    )
                )
            )
        )


class NarrativeBound:
    def __init__(self, start: 'Char', end: 'Char'):
        self.start = start
        self.end = end

    def __str__(self):
        return f"Bound: {self.start} {self.end}"


class Factory(abc.ABC):
    @classmethod
    @abc.abstractmethod
    def extract(cls, content: List[Part]):
        pass


class SubNarrativeFactory(Factory):
    class _InProgress:
        def __init__(self, index, bound: 'NarrativeBound'):
            self.index = index
            self.bound = bound

        def __str__(self):
            return f"Index: {self.index} {self.bound}"

    @classmethod
    def bounds(cls):
        return [
            NarrativeBound(Char("'"), Char("'")),
            NarrativeBound(Char('"'), Char('"')),
            NarrativeBound(Char('“'), Char('”')),
            NarrativeBound(Char('('), Char(')'))
        ]

    @classmethod
    def starts(cls) -> Dict['Char', 'NarrativeBound']:
        return {x.start: x for x in cls.bounds()}

    @classmethod
    def extract(cls, content: List[Part]) -> List['ComplexPart']:
        extracted = []

        in_progress = None
        for index, item in enumerate(content):
            if item in cls.starts() and not in_progress:
                in_progress = cls._InProgress(index, cls.starts()[item])
            elif in_progress and item == in_progress.bound.end:
                extracted.append(SubNarrative(content=content[in_progress.index + 1:index]))
                in_progress = None
            elif not in_progress:
                extracted.append(item)
        return extracted


class SubNarrative(Narrative):
    pass


class Word(SimpleSplit):

    @classmethod
    def allowed(cls):
        return Char,

    @property
    def parts(self):
        return WordFactory.extract(
            self.content
        )


class SplitFactory(Factory, abc.ABC):

    @classmethod
    @abc.abstractmethod
    def mapping(cls) -> List['Splitter']:
        pass

    @classmethod
    def map(cls) -> Dict[Tuple['Char'], str]:
        return {
            tuple(char.chars): char.modifier
            for char in cls.mapping()
        }

    @classmethod
    @abc.abstractmethod
    def part_cls(cls) -> Type['SimpleSplit']:
        pass

    @classmethod
    def chars(cls):
        return [list(char) for char in cls.map().keys()]

    @classmethod
    def extract(cls, content: List['Part']):
        splitted = []
        if any(contains(char, content) for char in cls.chars()):
            start = 0
            for index in range(len(content)):
                for char in cls.chars():
                    item = content[index:(len(char) + index)]
                    if item == char and index > 0:
                        for_include = content[start:index]
                        if all([isinstance(x, cls.part_cls().allowed()) for x in for_include]) and not any(contains(char, for_include) for char in cls.chars()):
                            splitted.append(cls.part_cls()(content=for_include, modifier=cls.map().get(tuple(item))))
                        else:
                            splitted.extend(for_include)
                        start = index + len(char)
                        break
            if content[start:]:
                for_include = content[start:]
                if all([isinstance(x, cls.part_cls().allowed()) for x in for_include]) and not any(contains(char, for_include) for char in cls.chars()):
                    splitted.append(cls.part_cls()(content=for_include))
                else:
                    splitted.extend(for_include)
        else:
            splitted = content

        return splitted


class SemiSentenceFactory(SplitFactory):

    @classmethod
    def mapping(cls):
        return [
            Splitter((Char(","), Char(" "))),
            Splitter((Char(","),)),
            Splitter((Char(":"), Char(" ")), "colon"),
            Splitter((Char(":"),), "colon"),
            Splitter((Char(";"), Char(" ")), "semicolon"),
            Splitter((Char(";"),), "semicolon")
        ]

    @classmethod
    def part_cls(cls) -> Type['SimpleSplit']:
        return SemiSentence


class SemiSentence(SimpleSplit):

    @classmethod
    def allowed(cls):
        return Word, Narrative, Char

    @property
    def parts(self):
        content = self.content

        # A single word semi sentence doesn't parse correctly without the space
        if all([isinstance(x, Char) for x in self.content]):
            content += " "

        return WordFactory.extract(content)


class SentenceFactory(SplitFactory):

    @classmethod
    def part_cls(cls) -> Type['SimpleSplit']:
        return Sentence

    @classmethod
    def mapping(cls) -> List['Splitter']:
        return [
            Splitter((Char("."), Char(" "))),
            Splitter((Char("."),)),
            Splitter((Char("!"), Char(" ")), "exclamation"),
            Splitter((Char("!"),), "exclamation"),
            Splitter((Char("?"), Char(" ")), "question"),
            Splitter((Char("?"),), "question"),
        ]


class Sentence(SimpleSplit):

    @classmethod
    def allowed(cls):
        return Word, Narrative, Char

    @property
    def parts(self):
        return WordFactory.extract(
            SemiSentenceFactory.extract(
                self.content
            )
        )


class ParagraphFactory(SplitFactory):

    @classmethod
    def part_cls(cls) -> Type['SimpleSplit']:
        return Paragraph

    @classmethod
    def mapping(cls) -> List['Splitter']:
        return [
            Splitter((Char("\n"),)),
        ]


class Paragraph(SimpleSplit):

    @classmethod
    def allowed(cls):
        return Sentence, Word, Narrative, Char

    @property
    def parts(self):
        return WordFactory.extract(
            SentenceFactory.extract(
                self.content
            )
        )


class WordFactory(SplitFactory):

    @classmethod
    def mapping(cls):
        return [
            Splitter((Char(" "),))
        ]

    @classmethod
    def part_cls(cls) -> Type['SimpleSplit']:
        return Word


class Root(Narrative):

    @classmethod
    def charize(cls, content: str):
        return [Char(value=x) for x in textwrap.dedent(content).strip()]

    @classmethod
    def allowed(cls):
        return Char,
