import json

from story_parser.models import Char, ComplexPart, Word, Sentence


class StorySerializer(json.JSONEncoder):
    def default(self, o):

        if isinstance(o, Word):
            return ''.join(o.value for o in o.parts)

        if isinstance(o, Char):
            return o.value

        if isinstance(o, Sentence) and o.modifier:
            return dict(
                type=o.__class__.__name__,
                content=o.parts,
                modifier=o.modifier
            )

        if isinstance(o, ComplexPart):
            return dict(
                type=o.__class__.__name__,
                content=o.parts
            )

        return super().default(o)
