import json

import pytest
import pprint
import textwrap
from story_parser.serializers import StorySerializer
from story_parser.models import Root, Char


@pytest.mark.parametrize(
    "input, expected",
    [
        (
                """
                but it had no pictures or conversations in it, “and what is the use of (an ugly and loud) book,” thought Alice.
                """,
                [
                    {
                        'content': [
                            {
                                'content': ['but', 'it', 'had', 'no', 'pictures', 'or', 'conversations', 'in', 'it'],
                                'type': 'SemiSentence'
                            },
                            {
                                'content': [
                                    {
                                        'content': [
                                            {
                                                'content': ['and', 'what', 'is', 'the', 'use', 'of',
                                                            {
                                                                'content': ['an', 'ugly', 'and', 'loud'],
                                                                'type': 'SubNarrative'
                                                            },
                                                            'book'
                                                            ],
                                                'type': 'SemiSentence'
                                            }
                                        ],
                                        'type': 'SubNarrative'
                                    },
                                    'thought',
                                    'Alice'
                                ]
                                ,
                                'type': 'SemiSentence'
                            }
                        ],
                        'type': 'Sentence'
                    }
                ]
        ),
        (
                """
                Then, they laughed.
                """,
                [
                    {
                        'content': [
                            {
                                'content': ['Then'],
                                'type': 'SemiSentence'
                            }, {
                                'content': ['they', 'laughed'],
                                'type': 'SemiSentence'
                            }
                        ],
                        'type': 'Sentence'
                    }
                ]
        ),
        (
                """
                This a test, It is good.
                """,
                [
                    {
                        'content': [
                            {
                                'content': ['This', 'a', 'test'],
                                'type': 'SemiSentence'
                            }, {
                                'content': ['It', 'is', 'good'],
                                'type': 'SemiSentence'
                            }
                        ],
                        'type': 'Sentence'
                    }
                ]
        ),
        (
                """
                Is this a test? It is good.
                """,
                [
                    {
                        'content': ['Is', 'this', 'a', 'test'],
                        'type': 'Sentence',
                        'modifier': 'question'
                    },
                    {
                        'content': ['It', 'is', 'good'],
                        'type': 'Sentence'
                    }
                ]
        ),
        (
                """
                This is a test. It is good.
                """,
                [
                    {
                        'content': ['This', 'is', 'a', 'test'],
                        'type': 'Sentence'
                    },
                    {
                        'content': ['It', 'is', 'good'],
                        'type': 'Sentence'
                    }
                ]
        ),
        (
                """Bob said 'This is a test.'.""",
                [
                    {
                        'content': [
                            'Bob',
                            'said',
                            {
                                'content': [
                                    {
                                        'content': ['This', 'is', 'a', 'test'], 'type': 'Sentence'
                                    }
                                ],
                                'type': 'SubNarrative'
                            }
                        ],
                        'type': 'Sentence'
                    }
                ]
        ),
        (
                """Bob said (This is a test.).""",
                [
                    {
                        'content': [
                            'Bob',
                            'said',
                            {
                                'content': [
                                    {
                                        'content': ['This', 'is', 'a', 'test'], 'type': 'Sentence'
                                    }
                                ],
                                'type': 'SubNarrative'
                            }
                        ],
                        'type': 'Sentence'
                    }
                ]
        ),
        (
                textwrap.dedent("""
                This is a test. It makes sense?
                It is good.
                """),
                [
                    {
                        'content': [
                            {'content': ['This', 'is', 'a', 'test'], 'type': 'Sentence'},
                            {'content': ['It', 'makes', 'sense'], 'type': 'Sentence', 'modifier': 'question'}
                        ],
                        'type': 'Paragraph'
                    },
                    {
                        'content': [
                            {'content': ['It', 'is', 'good'], 'type': 'Sentence'}
                        ],
                        'type': 'Paragraph'
                    }
                ]
        ),
    ]
)
def test_parse(input, expected):
    story = Root(content=[Char(value=x) for x in textwrap.dedent(input).strip()])
    generated = story.parts
    pp = pprint.PrettyPrinter(indent=4, compact=False, width=160)
    encoded = StorySerializer().encode(generated)
    decoded = json.loads(encoded)
    assert decoded == expected, decoded
